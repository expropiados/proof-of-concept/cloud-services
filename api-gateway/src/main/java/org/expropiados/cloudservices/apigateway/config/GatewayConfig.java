package org.expropiados.cloudservices.apigateway.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GatewayConfig {

    @Autowired
    AuthenticationFilter filter;

    @Bean
    public RouteLocator routes(RouteLocatorBuilder builder) {
        return builder.routes()
                .route("proof-micro-service", r -> r.path("/proof-micro-service/**")
                        .filters(f -> f.filter(filter))
                        .uri("lb://proof-micro-service"))

                .route("proof-micro-uuid-service", r -> r.path("/proof-micro-uuid-service/**")
                    .filters(f -> f.filter(filter))
                    .uri("lb://proof-micro-uuid-service"))

                .route("security-service", r -> r.path("/security-service/**")
                        .filters(f -> f.filter(filter))
                        .uri("lb://security-service"))
                .build();
    }

}
